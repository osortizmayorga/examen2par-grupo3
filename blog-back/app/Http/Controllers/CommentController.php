<?php

namespace App\Http\Controllers;
use App\Models\Comment;
use Illuminate\Http\Request;
use DB;
class CommentController extends Controller
{
    /**
     * Obtener una lista de todos los comments de un post
     */
    public function list($id)
    {   
        $comments = Comment::select()->where('post_id', '=', $id)->get();

        return response()->json($comments);
    }

    public function listarTodo($id){
        $comentario = DB::table('comments')

        ->join('users', 'users.id', '=', 'comments.user_id')

        ->select('comments.id','users.name', 'comments.content', 'comments.updated_at', 'comments.created_at')
        ->where('comments.post_id', '=', $id)

        ->get();

        return response()->json($comentario);
    }

    /**
     * Almacenar un nuevo comment
     */
    public function store(Request $request)
    {
        $request->validate([
            'post_id' => 'required',
            'content' => 'required',
            'user_id' => 'required'
        ]);

        Comment::create([
            'post_id' => $request->post_id,
            'content' => $request->content,
            'user_id' => $request->user_id
        ]);

        return response()->json([
            'message' => 'Successfully created comment!'
        ], 201);
    }

    /**
     * Mostrar un comment especifico
     */
    public function show($id)
    {
        $comment = Comment::find($id);
        return response()->json($comment);
    }

    /**
     * Actualizar un  comment
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);
        $comment->content = $request->content;
        $comment->save();

        return response()->json([
            'message' => 'Successfully updated comment!'
        ], 201);
    }

    /**
     * Eliminar un comment especifico
     */
    public function delete($id)
    {
        $comment = Comment::find($id);
        $comment->delete();
        return response()->json([
            'message' => 'Successfully deleted comment!'
        ]);
    }
}
