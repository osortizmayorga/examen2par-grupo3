<?php

namespace App\Http\Controllers;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Obtener una lista de todos los posts
     */
    public function list()
    {
        $posts = Post::all();
        return response()->json([
            'data' => $posts]);
    }

    /**
     * Almacenar un nuevo post
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'user_id' => 'required'
        ]);

        Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'user_id' => $request->user_id
        ]);

        return response()->json([
            'message' => 'Successfully created post!'
        ], 201);
    }

    /**
     * Mostrar un post especifico
     */
    public function show($id)
    {
        $post = Post::find($id);
        return response()->json($post);
    }

    /**
     * Actualizar un  post
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $post->title = $request->title;
        $post->content = $request->content;
        $post->save();

        return response()->json([
            'message' => 'Successfully updated post!'
        ], 201);
    }

    /**
     * Eliminar un post especifico
     */
    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();
        return response()->json([
            'message' => 'Successfully deleted post!'
        ]);
    }

}
