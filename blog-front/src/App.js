import './App.css';

import Login from './pages/Login/Login';
import Register from './pages/Registro/Register';
import Navbar from './components/Navbar.component';
import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Row, Col } from 'reactstrap';


import PostList from './components/PostList.component';
import CreatePost from './components/CreatePost.component';
import ShowPost from './components/ShowPost.component';

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />
        {/* <Home/> */}
        {/* <Login/> */}

          <Row>
            <Col md={12}  >
              <div className="wrapper bk">
                <Switch>
                  {/* <Route exact path='/' component={index} /> */}
                  <Route exact path="/" component={PostList} />
                  <Route path="/login" component={Login} />
                  <Route path="/register" component={Register} />
                  <Route path="/newpost" component={CreatePost} />
                  <Route path="/show" component={ShowPost} />
                </Switch>
              </div>
            </Col>
          </Row>

      </Router>
    </div>
  );
}

export default App;
