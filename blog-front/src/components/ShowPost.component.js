
import React from 'react';
import Post from './Post.component';


const ShowPost = ({ location }) => {

    return (
        <div>
            <Post props={location.state.id}/>
        </div>
    );
}
export default ShowPost;