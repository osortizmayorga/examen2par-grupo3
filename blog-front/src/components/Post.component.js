import { Component } from "react";
import React from 'react';
import Axios from "axios";
import { Link } from "react-router-dom";
import Comments from "./Comment";
import CreateComment from "./createComment";

let id_blog;
export default class Post extends Component {
    
    constructor(props) {
        super(props)
        id_blog = this.props.props;
        this.state = {
            id_blog: '',
            content: '',
            title: '',
            creation: '',
            user_id: ''
        }
    }
    componentDidMount() {
        this.getDatos();
    }

    getDatos() {

        let headers = {
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "Authorization": "Bearer " + sessionStorage.getItem('token')
        };
        let URL = 'http://127.0.0.1:8000/api/posts/show/' + id_blog;
        Axios.get(URL, { headers: headers })
            .then((res) => {
                this.setState({
                    id_blog: res.data.id,
                    content: res.data.content,
                    title: res.data.title,
                    creation: res.data.created_at,
                    user_id: res.data.user_id
                  });

            })
            .catch((error) => {

                console.log(error);
            })
       
    }




    render() {
        return (
            <div>
                <br />
                <div className="row" >
                    <div className="col-4 col-md-2"></div>
                    <div className="col-10 col-md-8">

                        <div className="card text-center">
                            <div className="card-body">
                                <h1 className="card-title">{this.state.title}</h1>
                                <p className="card-text"><small className="text-muted">{this.state.creation}</small></p>
                                <p className="card-text text-justify">{this.state.content}</p>

                            </div>
                            <div className="card-footer">
                                <CreateComment props={this.state.id_blog}/>
                                <Comments props={this.state.id_blog}/>
                            </div>
                            {/* { <div class="card-footer">
                     <small class="text-muted">id: {this.props.obj.id}</small>
                     </div>
                    <div class="card-footer">
                        <small class="text-muted">tilte: {this.props.obj.title}</small>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">content: {this.props.obj.content}</small>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">user_id: {this.props.obj.user_id}</small>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">created_at: {this.props.obj.created_at}</small>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">updated_at: {this.props.obj.updated_at}</small>
                    </div> } */}
                        </div>

                    </div>
                    <div className="col-4 col-md-2"></div>
                </div>
                <br />
            </div>
        );
    }
}