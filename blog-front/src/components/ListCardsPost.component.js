import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class CarTableRow extends Component {

    render() {
        return (
            <div>
                <br />
                <div className="row" >
                    <div className="col-4 col-md-2"></div>
                    <div className="col-10 col-md-8">

                        <div className="card text-center">
                            <div className="card-body">
                                <h1 className="card-title">{this.props.obj.title}</h1>
                                <p className="card-text text-justify">{this.props.obj.content}
                                <Link to={{
                                    pathname: '/show',
                                    state: { id: this.props.obj.id }
                                    }}>Leer mas</Link></p>
                                <p className="card-text"><small className="text-muted">{this.props.obj.created_at}</small></p>
                                
                            </div>
                            
                        </div>

                    </div>
                    <div className="col-4 col-md-2"></div>
                </div>
                <br />
            </div>
        )
    }
}