import React from 'react';
import {Link, NavLink} from 'react-router-dom';

const Navbar = () => {
    return (
        <div className="navbar navbar-dark bg-dark">
            <div className="container">
                <Link to="/" className="navbar-brand">Blog - Grupo 3</Link>
                <div>
                    
                    <div className="d-flex">
                        {sessionStorage.getItem('token') ? 
                        <NavLink 
                            className="btn btn-dark mr-3" 
                            to="/newpost"
                            exact
                        >
                            Nuevo Post
                        </NavLink>:
                        null} 
                        {!sessionStorage.getItem('token') ? 
                        <NavLink 
                            className="btn btn-dark mr-3" 
                            to="/login"
                            exact
                        >
                            Inicio Sesion
                        </NavLink>:
                        null}    
                        {!sessionStorage.getItem('token') ?                  
                        <NavLink 
                            className="btn btn-dark mr-3" 
                            to="/register"
                            exact
                        >
                            Registrate
                        </NavLink>:
                        null}
                        

                        
                    </div>
                </div>
            </div>
        </div>
       );
}

export default Navbar