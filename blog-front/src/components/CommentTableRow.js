import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
// import Button from 'react-bootstrap/Button';
// eslint-disable-next-line
import axios from 'axios';
import CreateComment from './createComment';
// import Swal from 'sweetalert2';




export default class commentTableRow extends Component {
    constructor(props) {
        
        super(props);
        this.deleteComment = this.deleteComment.bind(this);
    }
   cargar(){
       this.props.history.push('Comment')       
   }
      
    render() {
        return (
        <div>
            <tr>
                <td>{this.props.obj.id}</td>
                <td>{this.props.obj.content}</td>
                <td>{this.props.obj.name}</td>
                <td>{this.props.obj.created_at}</td>
                <td>{this.props.obj.updated_at }</td>
                
            </tr>
            
            </div>
        );
    }
}