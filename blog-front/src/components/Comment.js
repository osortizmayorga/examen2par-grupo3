import React, { Component } from "react";
import axios from 'axios';

import CommentTableRow from './CommentTableRow';
import { Table } from "reactstrap";
import CreateComment from "./createComment";


let id_post;
export default class CommentList extends Component {

  constructor(props) {
    
    super(props);
    id_post=this.props.props
    this.state = {
      comments: []
    };
  }
  getComments() {
    let URL = 'http://127.0.0.1:8000/api/comments/'+id_post;

    console.log(URL)
    let headers = {
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "Authorization": "Bearer " + sessionStorage.getItem('token')
    };
    axios.get(URL, {headers: headers})
    .then((res) => {
      console.log(res)
      this.setState({
        comments: res.data
      });
    })
    .catch((error) => {
      
      console.log(error);
    })
  }

  componentDidMount() {
    this.getComments();    
  }
 
  DataTable() {
    return this.state.comments.map((res, i) => {
      return <CommentTableRow obj={res} key={i} />;
    });
  }


  render() {
    return (<div className="table-wrapper">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Numero Comentario</th>
            <th>Comentario</th>
            <th>Author</th>
            <th>Creado</th>            
            <th>Actualizado</th>            
          </tr>
        </thead>
        <tbody>
          {this.DataTable()}
        </tbody>
      </Table>
    </div>);
  }
}