import React, { useState } from 'react';
import '../pages/Registro/Register.css';
import Title from '../pages/Login/components/Title/Title';
import Label from '../pages/Login/components/Label/Label';
import Input from '../pages/Login/components/Input/Input';
import TextArea from '../pages/Login/components/TextArea/TextArea';
import Axios from 'axios';
import { Redirect } from 'react-router-dom';

const CreatePost = () => {
    // user= var q almacena el estado, setUser encargado d almacenas
    // eslint-disable-next-line
    const [ isNuevo, setIsNuevo] = useState(false);
    const [ hasError, setHasError ] = useState(false);
    const [ toList, setToList ] = useState(false);

    const [title, setTitle] = useState('');
    const [titleError, setTitleError] = useState(false);
    const [contenido, setContenido] = useState('');
    const [contenidoError, setContenidoError] = useState(false);

    function handleChange(name, value) {
        // eslint-disable-next-line
        switch (name) {
            case 'titulo':
                setTitle(value);
                if (value.length < 2) {
                    setTitleError(true);
                } else { setTitleError(false) };
                setHasError(false);
                break;
            case 'cont':
                setContenido(value);
                if (value.length < 50) {
                    setContenidoError(false);
                } else { setContenidoError(true) };
                setHasError(false);
                break;
        }
    }
   

    function publish(title, contenido, id_user) {
        let data = {
            "title": title,
            "content": contenido,
            "user_id": 1
        }
        
        let headers = {
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "Authorization": "Bearer " + sessionStorage.getItem('token')
        }
        Axios.post('http://127.0.0.1:8000/api/posts/create', data,{
            headers: headers
        })
        // .then((res) => console.log(res))  
        .then((res) => {
            if(res.status === 201){
                setIsNuevo(true);
                setHasError(false);
                setToList(true)
                
            } else {

                setIsNuevo(false);
                setHasError(true);
            }
            
        })
        .catch((error) => {
        
        console.log(error);
        })
    };


    function ifMatch(param) {
        if(param.title.length > 0 && param.contenido.length > 0 ) {
            setIsNuevo(true);
            publish(param.title, param.contenido, param.id_user);
        } else{
            setHasError(true);
            setIsNuevo(false);
        }
 
    }

    function handleSubmit() {
        let account = { title, contenido}
        if (account) {
            //console.log('account:', account)
            ifMatch(account);
        }
    };

    // console.log('nombre:', user)
    //console.log('password:', password)

    return (
        <div className='register-container'>
            { toList ? <Redirect to="/" /> :null }
            <div className='register-content'>
                

                <Title text='Crear nuevo Post' />
                {hasError &&
                        <label className='label-alert'> Verifique que los campos no esten vacíos
                    </label>
                }
                <Label text='Titulo del Post' />
                <Input
                    attribute={{
                        id: 'titulo',
                        name: 'titulo',
                        type: 'text',
                        placeholder: 'Ingrese el titulo del post'
                    }}
                    handleChange={handleChange}
                    param={titleError}
                />
                 <Label text='Contenido' />
                <TextArea
                    attribute={{
                        id: 'cont',
                        name: 'cont',
                        placeholder: 'Ingrese contenido'
                    }}
                    handleChange = {handleChange}
                    param={contenidoError}
                />

                <div className='submit-button-container'>
                    <button onClick={handleSubmit} className='submit-button'>
                        Postear
                    </button>
                </div>
            </div>
        </div>

    )
};

export default  CreatePost;