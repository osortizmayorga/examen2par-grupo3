import React, { Component } from "react";

import axios from 'axios'
import Swal from 'sweetalert2';
// import { Button, Col, Form, Row } from "reactstrap";

let id_post;
export default class CreateComment extends Component {
  constructor(props) {
    super(props)
    id_post=this.props.props
    // Setting up functions
    this.onChangeCommentUserId = this.onChangeCommentUserId.bind(this);
    this.onChangeCommentPostId = this.onChangeCommentPostId.bind(this);
    this.onChangeCommentComment = this.onChangeCommentComment.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // Setting up state
    this.state = {
      user_id: '',
      post_id: '',
      comment: ''
    }
  }

  onChangeCommentUserId(e) {
    this.setState({ user_id: e.target.value })
  }

  onChangeCommentPostId(e) {
    this.setState({ post_id: e.target.value })
  }

  onChangeCommentComment(e) {
    this.setState({ comment: e.target.value })
  }

  onSubmit(e) {
    e.preventDefault()
    const comentario = {
      user_id: sessionStorage.getItem('id'),
      post_id: id_post,
      content: this.state.comment
    };
    let headers = {
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "Authorization": "Bearer " + sessionStorage.getItem('token')
    };
    axios.post('http://127.0.0.1:8000/api/comments/create', comentario, {headers:headers})
      .then(res => console.log(res.data));
    // console.log(`Car successfully created!`);
    // console.log(`Name: ${this.state.name}`);
    // console.log(`Price: ${this.state.price}`);
    // console.log(`Description: ${this.state.description}`);
      Swal.fire(
      'Buen trabajo!',
      'Comentario añadido correctamente',
      'success')
    

    this.setState({ user_id: '', post_id: '', comment: '' })
  }

  render() {
    return (<div className="form-wrapper">
      <form onSubmit={this.onSubmit}>

            <div class="form-group">
              <label for="exampleFormControlTextarea1">Comentar :D</label>
              <textarea class="form-control" value={this.state.comment}  id="exampleFormControlTextarea1" rows="3" onChange={this.onChangeCommentComment}></textarea>
              <button id="boton1" className="btn btn-primary" type="submit">Añadir comentario</button>
            </div>   
        </form>
        {/* <Form onSubmit={this.onSubmit}>
        <Row> 
            <Col>
             <Form.Group controlId="Name">
                <Form.Label>Usuario</Form.Label>
                <Form.Control type="number" pattern="[0-9]+" required title="Solo numeros" value={this.state.user_id} onChange={this.onChangeCommentUserId}/>
             </Form.Group>
            
            </Col>
            
            <Col>
             <Form.Group controlId="Price">
                <Form.Label>Autor</Form.Label>
                        <Form.Control type="number" pattern="[0-9]+" required title="Solo numeros" min="0" value={this.state.post_id} onChange={this.onChangeCommentPostId}/>
             </Form.Group>
            </Col>  
           
        </Row>
            

        <Form.Group controlId="content">
          <Form.Label>Comentario</Form.Label>
                <Form.Control as="textarea" type="textarea" value={this.state.comment} onChange={this.onChangeCommentComment}/>
        </Form.Group>

       
        <Button id="boton1" variant="primary" size="lg" block="block" type="submit">
          Añadir comentario
        </Button>
      </Form> */}
        <br></br>
        <br></br>


    </div>);
  }
}

