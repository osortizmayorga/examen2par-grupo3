import React, { Component } from "react";
import axios from 'axios';

import CardPost from './ListCardsPost.component';


export default class PostList extends Component {

  constructor(props) {
    
    super(props)
    this.state = {
      posts: [],
      cargado: false
    };
  }


  getPosts() {
    
    let headers = {
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest"}
    axios.get('http://127.0.0.1:8000/api/posts', {headers:headers} )
    // .then((res) => console.log(res.data))  
    .then((res) => {
      this.setState({
        posts: res.data,
        cargado: true
      });
    })
    .catch((error) => {
      
      console.log(error);
    })
  }

  componentDidMount() {
    this.getPosts();  
  }
 
  DataTable() {
    if (this.state.cargado){
      return this.state.posts.data.map((res, i) => {
        return <CardPost obj={res} key={i} />;
      });
    }
  }


  render() {
    return (<div className="list-group container">
              {this.DataTable()}
    </div>);
  }
}