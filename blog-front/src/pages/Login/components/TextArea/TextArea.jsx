import React from 'react';
import './TextArea.css';

const TextArea = ({attribute, handleChange, param}) => {
    return (
        <div className='textarea-container'>
            <textarea 
            id={attribute.id}
            name={attribute.name}
            placeholder={attribute.placeholder}
          
            rows={attribute.rows}
            onChange={ (e) => handleChange(e.target.name, e.target.value) }
            className={ param ? 'textarea-error' : 'regular-sty'}
            ></textarea>
        </div>
    )
};

export default TextArea;