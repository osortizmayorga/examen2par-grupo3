import React, { useState} from 'react';
import './Login.css';
import Title from './components/Title/Title';
import Label from './components/Label/Label';
import Input from './components/Input/Input';
import Axios from 'axios';
import { Redirect } from 'react-router-dom'

const Login = () => {

    const [ user, setUser] = useState('');
    const [ password, setPassword] = useState('');
    const [ passwordError, setPasswordError] = useState(false);
    // eslint-disable-next-line
    const [ isLogin, setIsLogin ] = useState(false);
    const [ hasError, setHasError ] = useState(false);
    const [ toHome, setToHome ] = useState(false);

    function handleChange(name, value) {
        if(name === 'usuario') {
            setUser(value);
            setHasError(false);
        } else {
            if(value.length < 8) { //pass
                setPasswordError(true);
                setHasError(false);
            } else {
                setPasswordError(false);
                setPassword(value);
                setHasError(false);
            }
        }
    };

    function authentication(user, pass) {
        let data = {
            "email": user,
            "password": pass,
            "remember_me": true
        }
        Axios.post('http://127.0.0.1:8000/api/auth/login', data)
        // .then((res) => console.log(res))  
        .then((res) => {
            if(res.status === 200){
                sessionStorage.setItem('token', res.data.access_token);
                sessionStorage.setItem('id', res.data.id);
                setIsLogin(true);
                setHasError(false);
                setToHome(true)
                
            } else {
                setToHome(false)
                setIsLogin(false);
                setHasError(true);
            }
            
        })
        .catch((error) => {
        
        console.log(error);
        })
    };

    function ifMatch(param) {
        if(param.user.length > 0 && param.password.length > 0) {
            authentication(param.user, param.password);
            // {isLogin ? 
            //     // console.log('llego')
            //     setIsLogin(true)
            //     // this.props.history.push('/');
            //     :
            //     setHasError(true);
            // }
        } else {
            setIsLogin(false);
            setHasError(true);
        }
    };

    function handleSubmit() {
        let account = { user, password }
        if(account){
            //console.log('account:', account)
            ifMatch(account);
        }
    };

   // console.log('usuario:', user)
    //console.log('password:', password)

    return(
        <div className='login-container'>
            {/* { isLogin ?  
                <>
                <h1> Hola, {user} </h1>
                <label>Felicidades, estas logueada.</label>
                </>
            : */}
            { toHome ? <Redirect to="/" /> :null }

            
            <div className='login-content'>
                <Title text='¡Bienvenido!' />
                { hasError &&
                    <label className='label-alert'> Su contraseña o usuario son incorrectos,
                        o no existen en nuestra plataforma
                    </label>
                }   
                <Label text= 'Usuario'/>
                <Input 
                attribute={{
                    id: 'usuario',
                    name: 'usuario',
                    type: 'text',
                    placeholder: 'Ingrese su usuario'
                }}
                handleChange={handleChange}
                />
                <Label text= 'Contraseña'/>
                <Input 
                attribute={{
                    id: 'contraseña',
                    name: 'contraseña',
                    type: 'password',
                    placeholder: 'Ingrese su contraseña'
                }}
                handleChange={handleChange}
                param={passwordError}
                />
                { passwordError &&
                    <label className='label-error'>
                        Contraseña inválida o incompleta
                    </label>
                }

                <div className='submit-button-container'>
                    <button onClick={handleSubmit} className='submit-button'>
                        Ingresar
                    </button>
                </div>
            </div>
            {/* // } */}
        </div>
            
    )
};

export default Login;