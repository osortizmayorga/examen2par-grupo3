import React, { useState } from 'react';
import './Register.css';
import Title from '../Login/components/Title/Title';
import Label from '../Login/components/Label/Label';
import Input from '../Login/components/Input/Input';
import Axios from 'axios';
import { Redirect } from 'react-router-dom';

const Register = () => {
    // user= var q almacena el estado, setUser encargado d almacenas
    const [user, setUser] = useState('');
    const [userError, setUserError] = useState(false);
    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState(false);

    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState(false);
    const [password2, setPassword2] = useState('');
    const [passwordError2, setPasswordError2] = useState(false);
    // eslint-disable-next-line
    const [isRegister, setIsRegister] = useState(false);
    const [ hasError, setHasError ] = useState(false);
    const [ toLogin, setToLogin ] = useState(false);



    function handleChange(name, value) {
        switch (name) {
            case 'nombre':
                setUser(value);
                if (value.length < 7) {
                    setUserError(true);
                } else { setUserError(false) };
                setHasError(false);
                break;
            case 'correo':
                setEmail(value);
                if (value.includes('@', 1)) {
                    setEmailError(false);
                } else { setEmailError(true) };
                setHasError(false);
                break;
            case 'contraseña':
                var pattern = new RegExp(/^(?=.*\d)(?=.*[\u0021-\u002b\u003c-\u0040])(?=.*[A-Z])(?=.*[a-z])\S{8,16}$/);
                setPassword(value);
                if (pattern.test(value)) {
                    setPasswordError(false);
                } else { setPasswordError(true) };
                setHasError(false);
                break;
            case 'contraseña2':
                setPassword2(value);
                if (password === value) {
                    setPasswordError2(false);
                } else { setPasswordError2(true) };
                setHasError(false);
                break;

            default:
                break;
        }
    }
   

    function register(user, email, pass) {
        let data = {
            "name": user,
            "email": email,
            "password": pass
        }
        let headers = {
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest"
        }
        Axios.post('http://127.0.0.1:8000/api/auth/signup', data,{
            headers: headers
        })
        // .then((res) => console.log(res))  
        .then((res) => {
            if(res.status === 201){
                setIsRegister(true);
                setHasError(false);
                setToLogin(true)
                
            } else {

                setIsRegister(false);
                setHasError(true);
            }
            
        })
        .catch((error) => {
        
        console.log(error);
        })
    };


    function ifMatch(param) {
        if(param.user.length > 0 && param.email.length > 0 && param.password.length > 0 && param.password2.length > 0) {
            setIsRegister(true);
            register(param.user, param.email, param.password);
        } else{
            setHasError(true);
            setIsRegister(false);
        }
 
    }

    function handleSubmit() {
        let account = { user, email, password , password2}
        if (account) {
            //console.log('account:', account)
            ifMatch(account);
        }
    };

    // console.log('nombre:', user)
    //console.log('password:', password)

    return (
        <div className='register-container'>
            { toLogin ? <Redirect to="/Login" /> :null }
            <div className='register-content'>
                <Title text='¡Regístrate!' />
                {hasError &&
                        <label className='label-alert'> Verifique que los campos no esten vacíos
                    </label>
                }
                <Label text='Nombre completo' />
                <Input
                    attribute={{
                        id: 'nombre',
                        name: 'nombre',
                        type: 'text',
                        placeholder: 'Ingrese su nombre completo'
                    }}
                    handleChange={handleChange}
                    param={userError}
                />
                <Label text='Correo' />
                <Input
                    attribute={{
                        id: 'correo',
                        name: 'correo',
                        type: 'text',
                        placeholder: 'Ingrese su correo electrónico'
                    }}
                    handleChange={handleChange}
                    param={emailError}
                />
                <Label text='Contraseña' />
                <Input
                    attribute={{
                        id: 'contraseña',
                        name: 'contraseña',
                        type: 'password',
                        placeholder: 'Ingrese su contraseña'
                    }}
                    handleChange={handleChange}
                    param={passwordError}
                />
                {passwordError &&
                        <label className='label-error'>
                            Mínimo un caracter especial, mayúscula, minúscula y número
                    </label>
                }
                <Label text='Confirmar contraseña' />
                <Input
                    attribute={{
                        id: 'contraseña2',
                        name: 'contraseña2',
                        type: 'password',
                        placeholder: 'Confirme su contraseña'
                    }}
                    handleChange={handleChange}
                    param={passwordError2}
                />
                {passwordError2 &&
                        <label className='label-error'>
                            Sus contraseñas no coinciden
                    </label>
                }
                <div className='submit-button-container'>
                    <button onClick={handleSubmit} className='submit-button'>
                        Registrar
                    </button>
                </div>
            </div>
        </div>

    )
};

export default Register;